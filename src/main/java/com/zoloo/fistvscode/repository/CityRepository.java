package com.zoloo.fistvscode.repository;

import com.zoloo.fistvscode.models.Player;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CityRepository extends CrudRepository<Player, Integer> {

}