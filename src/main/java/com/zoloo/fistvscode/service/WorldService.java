package com.zoloo.fistvscode.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.management.monitor.CounterMonitor;

import com.zoloo.fistvscode.mapper.TeamMapper;
import com.zoloo.fistvscode.models.Player;
import com.zoloo.fistvscode.models.TeamModel;
import com.zoloo.fistvscode.repository.CityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorldService {

    @Autowired
    private CityRepository cityRepository;

    @Autowired
    private TeamMapper teamMapper;

    @PostConstruct
    private void init(){
        
    }

    public List<TeamModel> getTeamPlayersById(int teamId) throws Exception {
        return teamMapper.getTeamPlayers(teamId);
    }

    public List<Player> getAllCity() throws Exception {
        List<Player> lstCity = new ArrayList<>();
        cityRepository.findAll().forEach(e -> lstCity.add(e));
        return lstCity;
    }
}