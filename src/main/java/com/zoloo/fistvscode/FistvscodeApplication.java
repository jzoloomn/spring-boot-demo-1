package com.zoloo.fistvscode;

import com.zoloo.fistvscode.Job.SampleJob;
import com.zoloo.fistvscode.Job.SchedulerConfig;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.TriggerBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

//@Import({ SchedulerConfig.class })
@SpringBootApplication
@EnableScheduling
public class FistvscodeApplication {

	public static void main(String[] args) throws SchedulerException {
		SpringApplication.run(FistvscodeApplication.class, args);
	}
}
