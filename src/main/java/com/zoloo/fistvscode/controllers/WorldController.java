package com.zoloo.fistvscode.controllers;

import java.util.ArrayList;
import java.util.List;

import com.zoloo.fistvscode.models.Player;
import com.zoloo.fistvscode.models.TeamModel;
import com.zoloo.fistvscode.repository.CityRepository;
import com.zoloo.fistvscode.service.WorldService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/world")
public class WorldController {

    @Autowired
    private WorldService worldService;

    @GetMapping(value = "/city")
    public List<Player> getAllCity() throws Exception {
        return worldService.getAllCity();
    }

    @GetMapping(value = "/getTeamPlayersById/{teamId}")
    public List<TeamModel> getTeamPlayersById(@PathVariable("teamId") final int teamId) throws Exception {
        return worldService.getTeamPlayersById(teamId);
    }
}