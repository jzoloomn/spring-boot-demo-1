package com.zoloo.fistvscode.Job;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.zoloo.fistvscode.service.WorldService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTask.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    
    @Autowired
    private WorldService worldService;

	@Scheduled(fixedRate = (60000*2))
	public void reportCurrentTime() throws Exception {
        // log.info("ZOLOO==> The time is now {}", dateFormat.format(new Date()));
        System.out.print("*****The time is now: " + dateFormat.format(new Date()));
        worldService.getTeamPlayersById(1).forEach(t-> System.out.print(t));
        System.out.println("\n");
	}
}