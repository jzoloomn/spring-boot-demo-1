package com.zoloo.fistvscode.mapper;

import java.util.List;

import com.zoloo.fistvscode.models.TeamModel;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TeamMapper {
    List<TeamModel> getTeamPlayers(int teamId);
}