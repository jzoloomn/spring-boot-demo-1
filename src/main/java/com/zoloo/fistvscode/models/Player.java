package com.zoloo.fistvscode.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "players")
public class Player {
    @Id
    @Column(name = "ID")
    private int ID;

    @Column(name = "PlayerName")
    private String name;
    
    @Column(name = "JerseyNumber")
    private int jerseyNumber;

    @Column(name = "PlayedYears")
    private int playedYears;

    @Column(name = "AvgPoints")
    private double avgPoints;

    @Column(name = "TeamId")
    private int teamId;

    public Player() {
    }

    public Player(int ID, String name, int jerseyNumber, int playedYears, double avgPoints, int teamId) {
        this.ID = ID;
        this.name = name;
        this.jerseyNumber = jerseyNumber;
        this.playedYears = playedYears;
        this.avgPoints = avgPoints;
        this.teamId = teamId;
    }

    public int getID() {
        return this.ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJerseyNumber() {
        return this.jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public int getPlayedYears() {
        return this.playedYears;
    }

    public void setPlayedYears(int playedYears) {
        this.playedYears = playedYears;
    }

    public double getAvgPoints() {
        return this.avgPoints;
    }

    public void setAvgPoints(double avgPoints) {
        this.avgPoints = avgPoints;
    }

    public int getTeamId() {
        return this.teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public Player ID(int ID) {
        this.ID = ID;
        return this;
    }

    public Player name(String name) {
        this.name = name;
        return this;
    }

    public Player jerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
        return this;
    }

    public Player playedYears(int playedYears) {
        this.playedYears = playedYears;
        return this;
    }

    public Player avgPoints(double avgPoints) {
        this.avgPoints = avgPoints;
        return this;
    }

    public Player teamId(int teamId) {
        this.teamId = teamId;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " ID='" + getID() + "'" +
            ", name='" + getName() + "'" +
            ", jerseyNumber='" + getJerseyNumber() + "'" +
            ", playedYears='" + getPlayedYears() + "'" +
            ", avgPoints='" + getAvgPoints() + "'" +
            ", teamId='" + getTeamId() + "'" +
            "}";
    }

 
}