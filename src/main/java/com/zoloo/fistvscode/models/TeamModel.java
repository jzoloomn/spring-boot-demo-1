package com.zoloo.fistvscode.models;

public class TeamModel {

    private int playerID;
    private String playerName;
    private int jerseyNumber;
    private int teamId;
    private String teamName;


    public TeamModel() {
    }

    public TeamModel(int playerID, String playerName, int jerseyNumber, int teamId, String teamName) {
        this.playerID = playerID;
        this.playerName = playerName;
        this.jerseyNumber = jerseyNumber;
        this.teamId = teamId;
        this.teamName = teamName;
    }

    public int getPlayerID() {
        return this.playerID;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getJerseyNumber() {
        return this.jerseyNumber;
    }

    public void setJerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public int getTeamId() {
        return this.teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public TeamModel playerID(int playerID) {
        this.playerID = playerID;
        return this;
    }

    public TeamModel playerName(String playerName) {
        this.playerName = playerName;
        return this;
    }

    public TeamModel jerseyNumber(int jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
        return this;
    }

    public TeamModel teamId(int teamId) {
        this.teamId = teamId;
        return this;
    }

    public TeamModel teamName(String teamName) {
        this.teamName = teamName;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " playerID='" + getPlayerID() + "'" +
            ", playerName='" + getPlayerName() + "'" +
            ", jerseyNumber='" + getJerseyNumber() + "'" +
            ", teamId='" + getTeamId() + "'" +
            ", teamName='" + getTeamName() + "'" +
            "}";
    }

}